# Better Scrape

Scrape websites via AWS lambda

# Development

Build the Docker image

```bash
docker build -t registry.gitlab.com/betterbets/better-scrape .
```

# Deploy

Build the Docker image

```bash
docker build -t registry.gitlab.com/betterbets/better-scrape .
```

Run a deploy

```bash
docker run -it -e STAGE=dev -e AWS_ACCESS_KEY_ID=123abc -e AWS_SECRET_ACCESS_KEY=123abc --rm registry.gitlab.com/betterbets/better-scrape deploy
```
