FROM python:3.7-alpine3.8

ARG SERVERLESS_VERSION=1.38.0
ARG SERVERLESS_PYTHON_REQUIREMENTS_VERSION=4.3.0

WORKDIR /scrape

# Install requirements
COPY requirements.txt /scrape
RUN apk add --update --no-cache --virtual build-deps \
    gcc \
    libffi-dev \
    musl-dev \
    openssl-dev \
 && pip install --no-cache-dir -r /scrape/requirements.txt \
 && apk del build-deps \
 && apk add --no-cache \
    bash \
    nodejs \
    nodejs-npm\
 # Serverless itself must be installed globally for the command to be
 # available
 && npm install -g serverless@$SERVERLESS_VERSION \
 # Serverless plugins must be installed in the local scope
 && npm install serverless-python-requirements@$SERVERLESS_PYTHON_REQUIREMENTS_VERSION

# Copy source
COPY entrypoint.sh /entrypoint.sh
COPY .pylintrc setup.cfg serverless.yml /scrape/
COPY resources /scrape/resources
COPY scrape /scrape/scrape

ENTRYPOINT ["bash", "/entrypoint.sh"]
CMD ["bash"]
