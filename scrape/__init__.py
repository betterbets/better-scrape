import logging

import cfscrape
from botocore.vendored import requests

MAX_RETRIES = 5
SCRAPER = cfscrape.create_scraper()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.getLogger('botocore').setLevel(logging.CRITICAL)


def handler(event, context):
    url = event['url']
    logging.info('Requesting url: "%s"', url)
    retry = 0
    while retry < MAX_RETRIES:
        try:
            response = SCRAPER.get(
                url
            )
            return {
                "statusCode": response.status_code,
                "body": response.content.decode('utf-8')
            }
        except (
                requests.exceptions.ChunkedEncodingError,
                requests.exceptions.ConnectionError,
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ProxyError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.SSLError,
                requests.exceptions.Timeout
        ):
            continue
    raise requests.Timeout
