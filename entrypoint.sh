#!/bin/bash
set -e

show_help() {
    echo """
Usage: docker run <imagename> COMMAND
Commands
bash    : Open a bash shell
lint    : Run lint checks
test    : Run Python tests
doc     : Build and run documentation
deploy  : Deploy to Lambda
destroy : Remove service
help    : Show this message
"""
}

case "$1" in
    bash)
        /bin/bash "${@:2}"
    ;;
    lint)
        pylint scrape/
        isort scrape/ --recursive --check-only --diff
    ;;
    deploy)
        serverless deploy
    ;;
    destroy)
        serverless remove
    ;;
    *)
        show_help
    ;;
esac
